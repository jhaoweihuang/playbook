# ThreatHunter Playbook script: Jupyter Environment Dockerfile
# Author: Roberto Rodriguez (@Cyb3rWard0g)
# License: GPL-3.0

FROM cyb3rward0g/jupyter-pyspark:0.0.4
LABEL maintainer="Roberto Rodriguez @Cyb3rWard0g"
LABEL description="Dockerfile ThreatHunter Playbook Project."

ARG NB_USER
ARG NB_UID
ENV NB_USER jovyan
ENV NB_UID 1000
ENV HOME /home/${NB_USER}
ENV PATH "$HOME/.local/bin:$PATH"

USER root

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}

USER ${NB_USER}

RUN python3 -m pip install openhunt==1.6.7 attackcti==0.3.0 pyarrow==0.17.1 --user
RUN git clone https://github.com/hunters-forge/mordor.git ${HOME}/mordor
RUN cd ${HOME}/mordor/datasets/small
RUN find . -type f -name "*.tar.gz" -print0 | xargs -0 -I{} tar xf {} -C .

COPY docs ${HOME}/docs

USER root

RUN chown ${NB_USER} /usr/local/share/jupyter/kernels/pyspark3/kernel.json \
    && cd ${HOME}/docs/ \
    && find . -type f -name "*.ipynb" -exec cp -n {} /opt/jupyter/notebooks \; \
    && cd ${HOME}/mordor \
    && find . -type f -name "*.ipynb" -exec cp -n {} /opt/jupyter/notebooks \; \
    && chown -R ${NB_USER}:${NB_USER} ${HOME} ${JUPYTER_DIR}

WORKDIR ${HOME}

USER ${NB_USER}